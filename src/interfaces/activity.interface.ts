export default interface IActivity {
  activity: string;
  accessibility?: number;
  type: string;
  participants: number;
  price: number;
  key?: string;
  link?: string;
  error?: string;
}
