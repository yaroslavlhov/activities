import axios from 'axios';

const http = axios.create();

http.defaults.baseURL = 'https://www.boredapi.com/';

export default http;
